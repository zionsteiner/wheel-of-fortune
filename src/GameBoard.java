import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Gameboard is an HBox display for the phrase in the game. It displays blanks under
 * each letter and updates to show correctly guessed letters
 * @author ZionSteiner
 */
public class GameBoard extends HBox {
    private String phrase;  // Mystery phrase to be guessed
    private Text[] text;    // Text array for phrase letters
    private Line[] blanks;  // Line array for blanks underneath letters
    private VBox[] cells;   // VBox to store letter with blank underneath

    /**
     * Builds GameBoard object. GameBoard is an HBox that holds an array of VBox "cells", each with a letter on top and
     * a line underneath.
     */
    GameBoard() {
        // Loads random phrase
        phrase = loadPhrase();

        text = new Text[phrase.length()];

        blanks = new Line[phrase.length()];

        cells = new VBox[phrase.length()];

        // Initializes text object for each letter
        for (int i = 0; i < phrase.length(); i++) {
            text[i] = new Text(phrase.substring(i, i + 1));
            text[i].setFont(Font.font("Verdana", FontWeight.BOLD, 20));
            text[i].setFill(Color.rgb(6, 50, 67));

            // Set invisible by default, to be set visible when correctly guessed
            text[i].setVisible(false);
            blanks[i] = new Line(0, 0, 20, 0);

            // Sets blanks under spaces invisible
            if (text[i].getText().equals(" ")) {
                blanks[i].setVisible(false);
            }

            // Add text and blanks to cell
            cells[i] = new VBox(text[i], blanks[i]);
            cells[i].setAlignment(Pos.CENTER);

            // Add cell to gameBoard
            getChildren().add(cells[i]);
        }

        setSpacing(5);
        setAlignment(Pos.CENTER);
    }

    /**
     * Load random phrase from file
     */
    private String loadPhrase() {
        ArrayList<String> phrases = new ArrayList<>();
        try (Scanner input = new Scanner(new File("Phrases.txt"))) {
            while (input.hasNext()) {
                phrases.add(input.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int rand = (int)(Math.random() * phrases.size());
        return phrases.get(rand);
    }

    public String getPhrase() {
        return phrase;
    }

    /**
     * Flash letters from color 1 to color 2, ending on color 2
     */
    public void flashLetters(List<Integer> letterPos, Color c1, Color c2) {
        Timeline animation = new Timeline(new KeyFrame(Duration.millis(250), e -> {
            for (int p : letterPos) {
                VBox v = (VBox)getChildren().get(p);
                Text t = (Text)v.getChildren().get(0);

                if (t.getFill().equals(c1)) {
                    t.setFill(c2);
                } else t.setFill(c1);
            }
        }));
        animation.setCycleCount(11);
        animation.play();
    }
}
