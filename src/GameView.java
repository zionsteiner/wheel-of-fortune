import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.util.ArrayList;

/**
 * GameView holds all WheelofFortune GUI components
 * and controls game logic.
 * @author ZionSteiner
 */
class GameView extends BorderPane {
    private Label lblOutput;                        // Label to display messages to user
    private Label lblRules;                         // Outlines game rules for player
    private Wheel wheel;                            // Wheel of fortune
    private GameBoard gameBoard;                    // Board to hold letters of mystery phrase
    private GuessInput input;                       // Section for player to enter guess
    private GameInfoDisplay infoDisplay;            // Displays basic game info
    private String phrase;                          // Mystery phrase to guess
    private Button btSpin;                          // Button to spin wheel
    private boolean gameWon = false;                // Tracks if game is over

    GameView() {
        // Initialize components
        wheel = new Wheel();
        gameBoard = new GameBoard();
        input = new GuessInput();
        infoDisplay = new GameInfoDisplay();
        phrase = gameBoard.getPhrase();
        btSpin = new Button("Spin");

        lblOutput = new Label("Spin the wheel to start the round");
        lblOutput.setFont(Font.font("Georgia", 15));

        lblRules = new Label("Rules:\n \t1. Spin the wheel\n" +
                "\t2. Guess a letter. If the guess is correct, \n" +
                "\tadd the value the wheel landed on to your score.\n" +
                "\tIf not, your score reverts to zero.\n" +
                "\t3. Repeat until you've guessed the entire phrase. Good luck!");

        // Button style from http://fxexperience.com/2011/12/styling-fx-buttons-with-css/
        btSpin.setStyle("-fx-background-color:\n" +
                "        linear-gradient(#f0ff35, #a9ff00),\n" +
                "        radial-gradient(center 50% -40%, radius 200%, #b8ee36 45%, #80c800 50%);\n" +
                "    -fx-background-radius: 6, 5;\n" +
                "    -fx-background-insets: 0, 1;\n" +
                "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.4) , 5, 0.0 , 0 , 1 );\n" +
                "    -fx-text-fill: #395306;");
        btSpin.setFont(Font.font("Arial", 20));
        btSpin.setOnAction(e -> {
            lblOutput.setText("Guess a letter");
            wheel.spin();
        });

        // Put wheel and spin button in vbox
        VBox wheelBox = new VBox(wheel, btSpin);
        wheelBox.setAlignment(Pos.CENTER);

        // Put wheelBox, input, and lblRules in hbox
        HBox centerScreen = new HBox();
        centerScreen.getChildren().addAll(wheelBox, input, lblRules);
        centerScreen.setSpacing(5);
        centerScreen.setAlignment(Pos.CENTER);

        // Put gameBoard and lblOutput in vbox
        VBox boardBox = new VBox(gameBoard, lblOutput);
        boardBox.setSpacing(10);
        boardBox.setAlignment(Pos.CENTER);

        setTop(infoDisplay);
        setAlignment(infoDisplay, Pos.CENTER);
        setCenter(centerScreen);
        setAlignment(centerScreen, Pos.CENTER);
        setBottom(boardBox);
        setAlignment(boardBox, Pos.CENTER);

        input.getTfInput().setOnAction(e -> play());
    }

    /**
     * If the wheel has been spun this round, runs game logic on guess entry
     */
    private void play() {
        ArrayList<String> guessedLetters = infoDisplay.getGuessedLetters(); // Stores list of guessedLetters from infoDisplay for easier access

        // Wheel must have already been spun before player can guess
        if (!gameWon && wheel.isWheelSpunThisRound()) {
            // Get character
            String guess = input.getText();

            // If nothing was entered, return
            if (guess.length() == 0) {
                return;
            }

            // If more than one character was entered, take the first
            if (guess.length() > 1) {
                guess = guess.substring(0, 1);
            }

            // Reset input textfield
            input.clear();

            // Input validation (letters only)
            String regex = "^[a-zA-Z]+$";
            if (!guess.matches(regex)) {
                lblOutput.setText("The aren't any of those in the phrase. Try a letter!");

                return;
            }

            // If the guess hasn't already been guessed, update list of guessedLetters held by infoDisplay
            guess = guess.toLowerCase();
            if (guessedLetters.contains(guess)) {
                lblOutput.setText("You've already guessed this letter!");
                return;
            } else infoDisplay.updateGuessedLetters(guess);

            // If guess is in phrase, show correct guess on board
            boolean guessInPhrase = false;
            for (int i = 0; i < phrase.length(); i++) {
                if (guess.equalsIgnoreCase(phrase.substring(i, i + 1))) {
                    VBox v = (VBox) gameBoard.getChildren().get(i);
                    Text t = (Text) v.getChildren().get(0);
                    t.setVisible(true);
                    lblOutput.setText("Correct! Spin again to begin next round.");
                    guessInPhrase = true;
                }
            }

            if (guessInPhrase) {
                int scoreIndex = wheel.getCurrIndex();
                int[] wheelPoints = wheel.getWheelPoints();
                infoDisplay.addScore(wheelPoints[scoreIndex]);
            }

            // Update incorrect guesses display and reset score
            if (!guessInPhrase) {
                lblOutput.setText("Incorrect! Spin again to begin next round.");
                infoDisplay.updateWrongLetters(guess);
                infoDisplay.resetScore();
            }

            // Set bool wheelSpunThisRound back to false for next round if game not won
            wheel.setWheelSpunThisRound(false);

            // Win: check if all letters have been guessed
            gameWon = true;
            for (int i = 0; i < phrase.length(); i++) {
                VBox v = (VBox) gameBoard.getChildren().get(i);
                Text t = (Text) v.getChildren().get(0);
                if (!t.getText().equals(" ") && !t.isVisible()) {
                    gameWon = false;
                    break;
                }
            }

            if (gameWon) {
                // Disable game until game is reset
                wheel.setWheelSpunThisRound(true);

                lblOutput.setText("Lucky guess. You win!\n" +
                        "Score at win: " + infoDisplay.getScore());

                ArrayList<Integer> letterPos = new ArrayList<>(phrase.length());
                for (int i = 0; i < phrase.length(); i++) {
                    letterPos.add(i);
                }

                // Flash letters from AggieBlue to green to indicate win
                gameBoard.flashLetters(letterPos, Color.rgb(6, 50, 67), Color.GREEN);

                // Open new game prompt
                offerNewGame();
            }
        }
    }

    /**
     * Prompt player to start new game
     */
    private void offerNewGame() {
        // Play again button
        Button play = new Button("PLAY AGAIN");
        play.setMaxHeight(Double.MAX_VALUE);

        // Exit game button
        Button exit = new Button("EXIT");
        exit.setMaxHeight(Double.MAX_VALUE);

        // Pane for buttons
        HBox options = new HBox(play, exit);
        options.setSpacing(5);
        options.setPadding(new Insets(5, 5, 5, 5));

        // Show menu to play again
        Stage playAgainStage = new Stage();
        playAgainStage.setScene(new Scene(options));
        playAgainStage.setResizable(false);
        playAgainStage.initStyle(StageStyle.UNDECORATED);
        playAgainStage.show();

        // Set play button to reset game
        play.setOnAction(e -> {
            playAgainStage.close();
            reset();
        });

        // Set exit button to exit the game
        exit.setOnAction(e -> System.exit(0));
    }

    /**
     * Reset GUI and game-state for new game
     */
    private void reset() {
        Stage primaryStage = (Stage) getScene().getWindow();
        Scene scene = getScene();
        GameView gameView = new GameView();
        scene.setRoot(gameView);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(scene.getHeight());
        primaryStage.setMinWidth(scene.getWidth());
        primaryStage.sizeToScene();
    }
}
