import javafx.animation.RotateTransition;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * Wheel class for wheelBase of fortune JavaFX game.
 * The wheelBase holds 8 point values that can be landed on when spun.
 * @author ZionSteiner
 */
public class Wheel extends Pane {
    private Circle wheelBase;                // Backboard of wheel
    private Line[] lines;                    // Lines to partition wheelBase
    private int numLines = 4;                // Number of lines to partition wheelBase
    private Polygon notch;                   // Notch to indicate where wheel lands
    private Group spinGroup;                 // Group of wheel nodes to spin during spin animation
    private int[] wheelPoints = {500, 400, 600, 200, 800, 300, 700, 100};    // Wheel point values
    private double currDegPos = 67.5;        // Current degree position keeps track of where on the board the notch is pointing
    private int currIndex;                   // Tracks the point index that notch lands on in wheelPoints
    private boolean wheelSpunThisRound = false; // Used to limit spins to once per round

    Wheel() {
        spinGroup = new Group();

        // Initialize wheelBase
        wheelBase = new Circle(100, Color.YELLOW);
        wheelBase.setStroke(Color.BLACK);
        wheelBase.centerXProperty().bind(widthProperty().divide(2));
        wheelBase.centerYProperty().bind(heightProperty().divide(2));
        spinGroup.getChildren().add(wheelBase);

        // Initialize lines
        lines = new Line[numLines];
        for(int i = 0; i < numLines; i++) {
            lines[i] = new Line();
            lines[i].startXProperty().bind(wheelBase.centerXProperty());
            lines[i].startYProperty().bind(wheelBase.centerYProperty().subtract(wheelBase.radiusProperty()));
            lines[i].endXProperty().bind(lines[i].startXProperty());
            lines[i].endYProperty().bind(lines[i].startYProperty().add(wheelBase.radiusProperty().multiply(2)));
            double degrees = 180 / numLines;
            lines[i].setRotate(degrees * i);
            spinGroup.getChildren().add(lines[i]);
        }

        // Initialize point labels
        Circle guide = new Circle(90);
        guide.centerXProperty().bind(wheelBase.centerXProperty().subtract(10));
        guide.centerYProperty().bind(wheelBase.centerYProperty().add(5));
        double degPerLbl = 360 / wheelPoints.length;

        // Calculate label x & y positions and rotation using basic trig
        for (int i = 0; i < wheelPoints.length; i++) {
            Text txtPoints = new Text(Integer.toString(wheelPoints[i]));
            txtPoints.xProperty().bind(guide.centerXProperty().add(guide.radiusProperty().multiply(Math.cos(Math.toRadians(i * degPerLbl + 45 / 2)))));
            txtPoints.yProperty().bind(guide.centerYProperty().subtract(guide.radiusProperty().multiply(Math.sin(Math.toRadians(i * degPerLbl + 45 / 2)))));
            txtPoints.setRotate(-(i * degPerLbl - 67.5));
            spinGroup.getChildren().add(txtPoints);
        }

        getChildren().addAll(spinGroup);

        // Initialize wheelBase notch
        DoubleProperty[] list = new DoubleProperty[6];
        SimpleDoubleProperty point1X = new SimpleDoubleProperty();
        point1X.bind(wheelBase.centerXProperty().add(wheelBase.radiusProperty().multiply(Math.cos(Math.toRadians(67.5)))));
        list[0] = point1X;

        SimpleDoubleProperty point1Y = new SimpleDoubleProperty();
        point1Y.bind(wheelBase.centerYProperty().subtract(wheelBase.radiusProperty().multiply(Math.sin(Math.toRadians(67.5)))));
        list[1] = point1Y;

        SimpleDoubleProperty point2X = new SimpleDoubleProperty();
        point2X.bind(point1X.add(20));
        list[2] = point2X;

        SimpleDoubleProperty point2Y = new SimpleDoubleProperty();
        point2Y.bind(point1Y);
        list[3] = point2Y;

        SimpleDoubleProperty point3X = new SimpleDoubleProperty();
        point3X.bind(point1X);
        list[4] = point3X;

        SimpleDoubleProperty point3Y = new SimpleDoubleProperty();
        point3Y.bind(point1Y.subtract(20));
        list[5] = point3Y;

        notch = new Polygon(point1X.doubleValue(), point1Y.doubleValue(), point2X.doubleValue(), point2Y.doubleValue(), point3X.doubleValue(), point3Y.doubleValue());
        notch.setRotate(-13);

        // Update notch position by listening to pane dimensions
        widthProperty().addListener(ov -> {
            ObservableList<Double> points = notch.getPoints();
            for (int i = 0; i < list.length; i++) {
                points.set(i, list[i].doubleValue());
            }
        });

        heightProperty().addListener(ov -> {
            ObservableList<Double> points = notch.getPoints();
            for (int i = 0; i < list.length; i++) {
                points.set(i, list[i].doubleValue());
            }
        });

        notch.setFill(Color.RED);
        getChildren().add(notch);
    }

    /**
     * 1. Spins wheelBase between 1 and 2 cycles
     * 2. Store the wheelPoints[] index of the point value the wheelBase landed on in currIndex
     */
    public void spin() {
        if (!wheelSpunThisRound) {
            wheelSpunThisRound = true;

            // 1. Spins wheelBase between 1 and 2 cycles
            double rand = Math.random() + 1;
            RotateTransition animation = new RotateTransition(Duration.millis(1000 * rand), spinGroup);
            animation.setByAngle(360 * rand);
            animation.play();

            // 2. Calculate and return score
            currDegPos += 360 * rand;
            currDegPos %= 360;
            currIndex = (int) currDegPos / 45;
        }
    }

    public int[] getWheelPoints() {
        return wheelPoints;
    }

    public int getCurrIndex() {
        return currIndex;
    }

    public boolean isWheelSpunThisRound() {
        return wheelSpunThisRound;
    }

    public void setWheelSpunThisRound(boolean wheelSpunThisRound) {
        this.wheelSpunThisRound = wheelSpunThisRound;
    }
}
