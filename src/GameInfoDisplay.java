import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

/**
 * GameInfoDisplay is the GUI element to display current game info.
 * It displays current score, high score, and incorrect guesses
 * @author ZionSteiner
 */
public class GameInfoDisplay extends VBox {
    private int score;                          // Game score
    private static int hiScore = 0;             // Highest score since game has been opened
    private Label lblCurScore;                  // Label to display current game's score
    private Label lblHiScore;                   // Label to display high score (since game has been opened)
    private Label lblWrongLetters;              // Label to display incorrect guesses
    private ArrayList<String> wrongLetters;     // List of incorrect guesses
    private ArrayList<String> guessedLetters;   // List of guessed letters, including incorrect guesses

    GameInfoDisplay() {
        guessedLetters = new ArrayList<>();
        wrongLetters = new ArrayList<>();
        score = 0;
        lblCurScore = new Label();
        lblHiScore = new Label();
        lblWrongLetters = new Label();

        lblCurScore.setText("Current Score: " + score);
        lblHiScore.setText("High Score: " + hiScore);
        lblWrongLetters.setText("Incorrect Guesses: ");

        getChildren().addAll(lblCurScore, lblHiScore, lblWrongLetters);
        setAlignment(Pos.CENTER);
        setSpacing(5);
    }

    /**
     * Updates score and score display
     * @param points change in score to be applied
     */
    public void addScore(int points) {
        score += points;

        if (score > hiScore) {
            hiScore = score;
        }

        lblCurScore.setText("Score: " + score);
        lblHiScore.setText("High Score: " + hiScore);
    }

    /**
     * Adds wrong guess to wrongLetters and updates display
     * @param l letter to add
     */
    public void updateWrongLetters(String l) {
        wrongLetters.add(l);

        StringBuilder s = new StringBuilder();
        s.append("Incorrect Guesses: ");
        for (String c : wrongLetters) {
            s.append(c.toUpperCase());
            s.append(" ");
        }
        lblWrongLetters.setText(s.toString());
    }

    /**
     * Adds letter to guessedLetters
     * @param l letter to add
     */
    public void updateGuessedLetters(String l) {
        guessedLetters.add(l);
    }

    public int getScore() {
        return score;
    }

    public void resetScore() {
        score = 0;
        lblCurScore.setText("Score: " + score);
    }

    public int getHiScore() {
        return hiScore;
    }

    public ArrayList<String> getWrongLetters() {
        return wrongLetters;
    }

    public ArrayList<String> getGuessedLetters() {
        return guessedLetters;
    }
}
