import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Simple WheelofFortune game implemented in JavaFX
 * @author ZionSteiner
 */
public class WheelofFortune extends Application {
    private GameView gameView;  // gameView object constructs GUI and controls game-state/logic

    @Override
    public void start(Stage primaryStage) {
        // Pane construction and game control
        gameView = new GameView();

        Scene scene = new Scene(gameView);
        primaryStage.setTitle("Wheel of Fortune");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setMinHeight(scene.getHeight());
        primaryStage.setMinWidth(scene.getWidth());
        primaryStage.sizeToScene();
    }
}
