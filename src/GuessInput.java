import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * GuessInput is the GUI element to enter phrase guesses
 * @author ZionSteiner
 */
public class GuessInput extends HBox {
    private Label lblInput;     // Input prompt
    private TextField tfInput;  // Textfield to enter guess

    GuessInput() {
        lblInput = new Label("Guess a letter: ");
        tfInput = new TextField();

        // Game logic will run when a letter is entered
        getChildren().addAll(lblInput, tfInput);
        setAlignment(Pos.CENTER);
    }

    public TextField getTfInput() {
        return tfInput;
    }

    public String getText() {
        return tfInput.getText();
    }

    public void clear() {
        tfInput.clear();
    }
}
